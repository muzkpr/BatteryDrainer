package Model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.batterydrainer.ShowResult;

/**
 * Created by mahmed on 09/03/2015.
 * <p/>
 * Class to store data in SQLite Database
 */
public class ModelHelper extends SQLiteOpenHelper {
    // Database version increase in case to update database
    private static int version = 1;
    // Database name
    private static String db_name = "drainer_game";
    // Table name
    private static String table_name = "secret";
    // column name
    private static String first_col = "_id";
    // column name
    private static String second_col = "value";
    // Refernce to database
    SQLiteDatabase database;

    public ModelHelper(Context context) {

        super(context, db_name, null, version);
    }
    // create table for the first time with column and its properties.
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("CREATE TABLE " + table_name + " (" + first_col + " INTEGER PRIMARY KEY AUTOINCREMENT, " + second_col + " VARCHAR(255))");
            database = db;
        } catch (Exception e) {
            ShowResult.showLog("exception which creating db" + e);

        }


    }
    // if any update required to database.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    // insert values in table
    public void insertValue(String value) {
        ShowResult.showLog(value);
        try {
            database.execSQL("INSERT INTO " + table_name + " (" + second_col + ") VALUES('" + value + "');");
        } catch (Exception e) {
            ShowResult.showLog(e.toString());
        }
    }
    // return results of query as string object. for this specific app we need only first record from table all the time that's why limit is 1
    public String getValue() {
        String sec;
        Cursor c = database.rawQuery("SELECT '" + second_col + "' FROM " + table_name + " LIMIT 1 ;", null);
        if (c.moveToFirst()) {
            sec = c.getString(0);
            ShowResult.showLog("able to select data from db " + sec);

            // Deleting record if found
//            db.execSQL("DELETE FROM student WHERE rollno='"+editRollno.getText()+"'");
//            showMessage("Success", "Record Deleted");
        } else {
            sec = "nothing found";
            ShowResult.showLog("Unable to select data from db");
        }
        return sec;

    }
    // delete record from table.
    public void delValue(String value) {


        ShowResult.showLog("going to delete from db " + value);

        // Deleting record if found
        database.execSQL("DELETE FROM " + table_name + " WHERE " + second_col + "='" + value + "'");
//            showMessage("Success", "Record Deleted");


    }


}
