package HttpService;

import com.example.batterydrainer.ShowResult;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
/**
 * Created by Muzamil.
 * HTTP manager to communicate with server using HttpURLConnection
 */
public class MyHttpManager {

    public static String getData(String uri) {
        ShowResult.showLog("inside HTTP My Manager");

        BufferedReader reader = null;
        try {
            // creat URL object from input uri
            URL url = new URL(uri);
            // Open HTTP url connection.
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            //Create String builder object to read input stream from server
            StringBuilder sb = new StringBuilder();
            // Read input stream from server
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            // read line by line the input stream
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            //convert and return string buffer as String
            return sb.toString();
        }
        // catch if any exception occur
        catch (Exception e) {
            ShowResult.showLog("Exception while connecting to server" + e.toString());
            return "notconnected";
        }

    }

}
