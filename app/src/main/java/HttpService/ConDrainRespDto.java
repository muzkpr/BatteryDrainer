package HttpService;

import com.example.batterydrainer.ShowResult;
/**
 * Created by Muzamil.
 * Data transfer object to store information received after submitting successful drain report.
 */

public class ConDrainRespDto {
    // store success message received from server
    private String success;
    // store bid value received from server
    private String bidval;
    // store total winning amount for all drains completed successfully.
    private String tally;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        ShowResult.showLog("inside Confirm Drain DTO set success" + success);
        if (success == null) {
            success = "nothing received in success";
        }
        this.success = success;
    }

    public String getBidval() {
        return bidval;
    }

    public void setBidval(String bidval) {
        if (bidval == null) {
            bidval = "no bidval received";
        }
        this.bidval = bidval;
    }

    public String getTally() {
        return tally;
    }

    public void setTally(String tally) {
        if (tally == null) {
            tally = "no tally received";
        }
        this.tally = tally;
    }
}
