package HttpService;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Muzamil on 20-Feb-15.
 * JSON parser to parse message received from sever.
 */
public class DidIWinJSONParser {

    final static String TAG = "BDService";

    // this method will receive json object as input and return a data transfer object (DidIWinRespDto)
    public static DidIWinRespDto parseResp(String content) {

        Log.d(TAG, "inside parser" + content);

        int outcome;
        String message;
        String secret;
        String amount;
        DidIWinRespDto resp = new DidIWinRespDto();
        try {
            JSONObject jb = new JSONObject(content);
//			JSONArray ar=new JSONArray(content);
//			JSONObject jb=ar.getJSONObject(0);

            // check if response from server contains any outcome then set it as outcome value for DidIWinRespDto
            if (jb.has("outcome")) {
                outcome = Integer.parseInt(jb.getString("outcome"));
            } else {
                outcome = 0;

            }
            // check if response from server contains any message then set it as message string for DidIWinRespDto
            if (jb.has("message")) {
                message = jb.getString("message");
            } else {
                message = "message not received";
            }
            // check if response from server contains any secret then set it as secret value for DidIWinRespDto
            if (jb.has("secret")) {
                secret = jb.getString("secret");


            } else {
                secret = "secret not received";

            }
            // check if response from server contains any amount then set it as amount value for DidIWinRespDto
            if (jb.has("amount")) {
                amount = jb.getString("amount");


            } else {
                amount = "amount not received";

            }
            // set outcome in DidIWinRespDto
            resp.setOutcome(outcome);

            if (outcome == 1) {
                // if outcome is 1 then set all other values in DidIWinRespDto
                resp.setMessage(message);
                resp.setSecret(secret);
                resp.setAmount(amount);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "exception in parser " + e);

            return resp;
        }
        return resp;
    }
}