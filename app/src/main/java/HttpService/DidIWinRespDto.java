package HttpService;

import com.example.batterydrainer.ShowResult;

/**
 * Created by Muzamil on 20-Feb-15.
 * Data transfer object for message received from sever.
 */
public class DidIWinRespDto {
    public static String secret;
    private int outcome;
    private String message;
    private String amount;

    // return outcome
    public int getOutcome() {
        return outcome;
    }

    // set outcome
    public void setOutcome(int outcome) {
        ShowResult.showLog("inside DTO set outcome" + outcome);

        this.outcome = outcome;
    }

    // return message
    public String getMessage() {
        return message;
    }

    //set message if no message received then set message as "no message received"
    public void setMessage(String message) {
        ShowResult.showLog("inside DTO set meessage" + message);
        if (message == null) {
            message = "no message received";
        }
        this.message = message;
    }

    // return secret
    public String getSecret() {
        return secret;
    }

    // set secret if no secret received then set "no secret received"
    public void setSecret(String secret) {
        if (secret == null) {
            secret = "no secret received";
        }
        this.secret = secret;
    }

    // return amount
    public String getAmount() {
        return amount;
    }

    // set secret if no amount received then set "no amount received"
    public void setAmount(String amount) {
        if (amount == null) {
            amount = "no amount received";
        }
        this.amount = amount;
    }
}
