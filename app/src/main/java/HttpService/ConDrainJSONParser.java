package HttpService;

import com.example.batterydrainer.ShowResult;

import org.json.JSONObject;
/**
 * Created by Muzamil.
 * Json parser to parse response from server after submitting successful drain report.
 */
public class ConDrainJSONParser {


    public static ConDrainRespDto parseResp(String content) {
        //just for debug purpose
        ShowResult.showLog("inside Confirm drain parser" + content);
        // store success message received from server
        String success;
        // store bid value received from server
        String bidval;
        // store total winning amount for all drains completed successfully.
        String tally;
        ConDrainRespDto resp = new ConDrainRespDto();
        try {
            JSONObject jb = new JSONObject(content);
//			JSONArray ar=new JSONArray(content);
//			JSONObject jb=ar.getJSONObject(0);
            if (jb.has("success")) {
                success = jb.getString("success");

            } else {
                success = "nothing";
            }
            if (jb.has("bidval")) {
                bidval = jb.getString("bidval");

            } else {
                bidval = "nothing";
            }
            if (jb.has("tally")) {
                tally = jb.getString("tally");

            } else {
                tally = "nothing";
            }
            resp.setSuccess(success);
            resp.setBidval(bidval);
            resp.setTally(tally);
        } catch (Exception e) {
            e.printStackTrace();
            ShowResult.showLog("exception in Confirm Drain parser " + e);

            return resp;
        }
        return resp;
    }
}