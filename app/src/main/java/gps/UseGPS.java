package gps;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
/**
 * Created by Muzamil.
 * use GPS to drain mobile battery
 */
public class UseGPS implements LocationListener {
    LocationManager locManager;
    // start location listener to get location information without any delay.
    public boolean startGPS(LocationManager locManager) {
        //get location manager
        this.locManager = locManager;
        // use GPS as location information provider.
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        return true;

    }
    //after completing the battery drain stop location listener
    public void stopGPS(LocationManager locManager) {
        try {
            locManager.removeUpdates(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

}
