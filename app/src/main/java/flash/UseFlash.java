package flash;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
/**
 * Created by Muzamil.
 * use flash to drain mobile battery
 */
public class UseFlash {
    // reference to camera
    Camera cam;

    public boolean startFlash() {
        try {
            // we use flash with camera
            cam = Camera.open();
            //aquire lock on camera
            cam.lock();
            Parameters p = cam.getParameters();
            // set flash mode to switch on the flash
            p.setFlashMode(Parameters.FLASH_MODE_TORCH);
            cam.setParameters(p);
            // start the flash
            cam.startPreview();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }


        return false;

    }
    // after completing the drain stop flash unlock camera and release it for other applications.
    public void stopFlash() {
        try {
            cam.unlock();
            cam.stopPreview();
            cam.release();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
