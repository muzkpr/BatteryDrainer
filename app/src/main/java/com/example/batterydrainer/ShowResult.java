package com.example.batterydrainer;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
/**
 * Created by Muzamil.
 * rather than creating tag in every class and importing log and toast class in every class I have created this class class and use it to show log and toast for uniformity.
 */
public class ShowResult {

    public static void showLog(String message) {
        Log.d("BDService", message);
    }

    public static void showToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
