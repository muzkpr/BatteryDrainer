package com.example.batterydrainer;


import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Muzamil.
 * activity to show confirmation to user after completing battery drain
 */
public class ConfirmDrainActivity extends ActionBarActivity {
    Button btOk;
    TextView tvMsg;
    String action;
    RemainingDrains rmD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // play notification sound while displaying popup

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // show message to user as popup window
        setContentView(R.layout.activity_popup);
        Intent intent = getIntent();
        String message = intent.getStringExtra("msg");
        action = intent.getStringExtra("action");
        rmD = new RemainingDrains();

        ShowResult.showLog(message + " action = " + action);
        tvMsg = (TextView) findViewById(R.id.tvMsg);
        tvMsg.setText(message);
        btOk = (Button) findViewById(R.id.btPopOk);
        btOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // check if user clicks the ok button then stop the drainer server.
                if (action.equalsIgnoreCase("report")) {
                    stopService(new Intent(getBaseContext(), BatteryDrainerService.class));
                    // check the remaining drain que from application context
                    int rd_value = rmD.getRemainDrains(getApplicationContext());
                    ShowResult.showLog("calling popup for remaining drain " + rd_value);
                    // if any remaining drain found show another pop up to inform user about starting the pending drain
                    if (rd_value > 0) {
                        ShowResult.showLog("calling popup for remaining drain " + rd_value);
                        String message = "Starting pending battery drain";
                        Intent in = new Intent().setClass(getBaseContext(), PopupActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        in.putExtra("msg", message);
                        in.putExtra("action", "anotherdrain");
                        startActivity(in);
                        // remove remaining drain from que
                        rmD.setRemainingDrains(getApplicationContext(), rd_value - 1);

                    }
                }
                //destroy activity on button click
                finish();
            }
        });
    }


}
