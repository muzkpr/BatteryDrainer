package com.example.batterydrainer;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.IBinder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import HttpService.DidIWinJSONParser;
import HttpService.DidIWinRespDto;
import HttpService.MyHttpManager;

/**
 * Created by Muzamil.
 * background service to collect battery info and communicate with server to check if this user won the bid
 */

public class BatteryInfoService extends Service {
    static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
    static String uuid;
    static int batLevel;

    // broadcast receiver to receive system broad cast to collect information about battery
    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Calendar currentTime = cal.getInstance();
            // get current battery level
            batLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        }


    };
    static boolean startDrain = true;
    SimpleDateFormat sdf;
    Calendar cal;
    String wMessage;
    String dMessage;
    String uri_get_resp;
    int outcome;

    @Override
    public void onCreate() {
        // register the batteryInfoReceiver
        this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        // unique user id to identify user uniquely on web server
        uuid = getUserId();
        // create uri string to communicate with server.
        uri_get_resp = "uri" + uuid;
        outcome = 0;
        sdf = new SimpleDateFormat(DATEFORMAT);
        // record time in universal time zone
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        super.onCreate();
    }

    @Override
    public void onDestroy() {
        // at application closure unregister receiver to avoid any memory leak
        unregisterReceiver(batteryInfoReceiver);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ShowResult.showLog("batery info service started");
        // calling internal method to communicate with server.
        getRespFromServer();
        return super.onStartCommand(intent, flags, startId);
    }


    private void getRespFromServer() {
        // creating a background AsyncTask because communicating with server requires internet call which can delay for few seconds
        MyAsyncClient asyncClient = new MyAsyncClient();
        // executing the aysnc task with uri as input parameter.
        asyncClient.execute(uri_get_resp);

    }

    private void showPopup(String message) {
        ShowResult.showLog("inside show popup Messaeg = " + message);
        // because we cannot create any UI object in background service so we start another activity to show popup to user
        Intent in = new Intent().setClass(getBaseContext(), PopupActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // send message to be displayed on activity window
        in.putExtra("msg", message);
        in.putExtra("action", "drain");
        startActivity(in);

    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    // get unique user id from content provider of aware process, which is already created by aware framework
    private String getUserId() {
        String auri = "content://com.aware.provider.aware/aware_device";
        Uri u = Uri.parse(auri);
        Cursor esm_data = getContentResolver().query(u, null, null, null, null);

        if (esm_data != null && esm_data.moveToLast()) {
            String uid = esm_data.getString(esm_data.getColumnIndex("device_id"));
            ShowResult.showLog(uid);
            return uid;

        }
        return null;
    }

    // aysnc task to communicate with server, we are expecting string as parameters and returns.
    private class MyAsyncClient extends AsyncTask<String, String, String> {
        // communicate with server in background thread, we will collect uri which we send to async task as first element of params array
        @Override
        protected String doInBackground(String... params) {
            // use MyMyHttpManager class to communicate with server and receive response as string from server
            String respFromServer = MyHttpManager.getData(params[0]);
            // retry for 30 seconds if failed to make connection with server.
            int retrycount = 0;
            while (respFromServer.equalsIgnoreCase("notconnected") && retrycount < 5) {
                retrycount++;
                try {
                    Thread.sleep(60 * 1000);
                    respFromServer = MyHttpManager.getData(params[0]);
                    ShowResult.showLog("Exception in connection " + respFromServer + " retry count= " + retrycount);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return respFromServer;
        }

        // after completing the background thread sent response back
        @Override
        protected void onPostExecute(String result) {
            DidIWinJSONParser jp = new DidIWinJSONParser();
            DidIWinRespDto rd = jp.parseResp(result);

            ShowResult.showLog("outcome " + rd.getOutcome() + "\nMessage " + rd.getMessage() + "\nSecret " + rd.getSecret() + "\nAmount " + rd.getAmount());

            if (rd.getOutcome() == 1) {

                showPopup(rd.getMessage() + "\nAmount " + rd.getAmount());
            }
//			super.onPostExecute(result);
        }

    }

}
