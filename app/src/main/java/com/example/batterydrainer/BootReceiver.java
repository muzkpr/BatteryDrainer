package com.example.batterydrainer;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

/**
 * Created by Muzamil on 20-Feb-15.
 * To start application automatically after phone restart
 */
public class BootReceiver extends BroadcastReceiver {
    Calendar cal;
    Intent serviceIntent;
    PendingIntent pintent;
    AlarmManager alarm;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            // set a notification to inform user about app start
            startNotification(context);

            setAlarm(context, intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // set an alarm to ping server after every hour. we used alarm service because it normal background services can be killed for memory management.
    private void setAlarm(Context context, Intent intent) {
        // get calendar instance to get time
        cal = Calendar.getInstance();
        serviceIntent = new Intent(context, BatteryInfoService.class);
        pintent = PendingIntent.getService(context, 0, serviceIntent, 0);
        alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        // invoke background service after every hour.
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 10 * 60 * 1000, pintent);

    }

    // set notification properties
    public void startNotification(Context context) {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        Notification notification = new Notification.Builder(context)
                .setContentTitle("Battery Drainer started")
                .setSmallIcon(R.drawable.ic_launcher)
                .build();
        mNotificationManager.notify(1, notification);

    }

}