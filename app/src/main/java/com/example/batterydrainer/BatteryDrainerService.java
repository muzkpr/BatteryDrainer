package com.example.batterydrainer;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import HttpService.ConDrainJSONParser;
import HttpService.ConDrainRespDto;
import HttpService.DidIWinRespDto;
import HttpService.MyHttpManager;
import audio.UseAudio2;
import camera.UseCamera;
import flash.UseFlash;
import gps.UseGPS;
import sensors.UseAllAvailableSensors;

public class BatteryDrainerService extends Service {
    static String uuid;
    LocationManager locManager;
    FileOutputStream fos;
    UseCamera uCam;
    UseGPS uGPS;
    UseFlash uFlash;
    UseAudio2 useAudio2;
    SimpleDateFormat sdf;
    Calendar cal;
    RemainingDrains rem_drains;

    String uri_submit_drain;

    @Override
    public void onCreate() {
        Log.d("Battery", "drain battery service created ");
        // create simple date format object to store time in simple format (not in milliseconds).
        sdf = new SimpleDateFormat(BatteryInfoService.DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        // create remaining drain object to get remaining drain values from application context
        rem_drains = new RemainingDrains();
        uuid = getUserId();
        uri_submit_drain = "uri.php";
        super.onCreate();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    public void stopService() {

        stopSelf();
    }

    public void stopDrain(String queryString) {
//        stopService(new Intent(getBaseContext(),UseAccelerometer.class));
//        stopService(new Intent(getBaseContext(),UseGravity.class));
//        stopService(new Intent(getBaseContext(),UseGyroscope.class));
//        stopService(new Intent(getBaseContext(),UseLinearAcceleration.class));
//        stopService(new Intent(getBaseContext(),UseProximity.class));
//        stopService(new Intent(getBaseContext(),UseRotationVector.class));

        try {
//			uCam.stopCamera();
            useAudio2.stop();
            uFlash.stopFlash();
            uGPS.stopGPS(locManager);
            stopService(new Intent(getBaseContext(), UseAllAvailableSensors.class));

        } catch (Exception e) {
            e.printStackTrace();
        }

        ShowResult.showLog("submit drain report " + queryString);

        MyAsyncClientSubmitReport asyncReportDrain = new MyAsyncClientSubmitReport();
        asyncReportDrain.execute(queryString);

//	}
//	public void showDrainReport(String message){
//
//		Intent in=new Intent().setClass(getBaseContext(),ConfirmDrainActivity.class);
//		in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		in.putExtra("msg", message);
//		in.putExtra("action", "report");
//		startActivity(in);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        ShowResult.showLog("should I start drain =" + BatteryInfoService.startDrain + "\nvalues in db ");

        //If there is no drain activity going on then start draining battery now
        if (BatteryInfoService.startDrain) {
            drainBattery();

        }
        // else if battery draining activity is already in process then add current activity in que
        else {
            int rd_value = rem_drains.getRemainDrains(getApplicationContext());
            int remaining_drains = rd_value + 1;
            rem_drains.setRemainingDrains(getApplicationContext(), remaining_drains);
            ShowResult.showLog("calling popup for remaining drain " + remaining_drains);

        }
        return super.onStartCommand(intent, flags, startId);
    }

    // method to start all services to drain mobile phone battery
    private void drainBattery() {
        //Change flag status to false to stop concurrent draining process.
        BatteryInfoService.startDrain = false;
        Calendar currentTime = cal.getInstance();
        ShowResult.showLog("drain battery started" + sdf.format(currentTime.getTime()));
        uFlash = new UseFlash();
//		uCam=new UseCamera();
        uGPS = new UseGPS();
        useAudio2 = new UseAudio2();
        locManager = (LocationManager) getBaseContext().getSystemService(LOCATION_SERVICE);
        // start recording sound using microphone
        useAudio2.start();
        // start gps service
        uGPS.startGPS(locManager);
        uFlash.startFlash();
        startService(new Intent(getBaseContext(), UseAllAvailableSensors.class));

        // start draining battery in background thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                // battery level when draining activity started
                int startBatLevel = BatteryInfoService.batLevel;
                // for this study we need to drain 10% of phone battery
                int requiredBatLevel = BatteryInfoService.batLevel - 10;
                Calendar currentTime = cal.getInstance();
                String startTime = sdf.format(currentTime.getTime());
                Long startTimeMili = currentTime.getTimeInMillis();
                //the secret key for secure handshake with server and to uniquely find the bid of winner
                String secret = DidIWinRespDto.secret;
//                startService(new Intent(getBaseContext(), UseAccelerometer.class));
//                startService(new Intent(getBaseContext(), UseGravity.class));
//                startService(new Intent(getBaseContext(), UseGyroscope.class));
//                startService(new Intent(getBaseContext(), UseLinearAcceleration.class));
//                startService(new Intent(getBaseContext(), UseProximity.class));
//                startService(new Intent(getBaseContext(), UseRotationVector.class));

                // check until draining activity drain battery level to the required battery level. sleep for 30 seconds
                while (BatteryInfoService.batLevel > requiredBatLevel) {
                    try {
                        Thread.sleep(30000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ShowResult.showLog("inside battery drainer while started bat level " + startBatLevel + " required bat level " + requiredBatLevel + " current bat level " + BatteryInfoService.batLevel);
                }

                // after completing draining activity create query string to submit results to server.
                String queryString = urlBuilder(uri_submit_drain, uuid, secret, startTimeMili, Calendar.getInstance().getTimeInMillis(), startBatLevel, BatteryInfoService.batLevel);
                // stop all services to stop draining.
                stopDrain(queryString);

            }
        }).start();

//		MyAsyncClientDrain asyncClientDrain=new MyAsyncClientDrain();
//
//		asyncClientDrain.execute();
    }

    // show response from server after successfully submitting draining results
    //because we can use UI elements in background threads so we will use another activity as popup to show results.
    public void showDrainReportPopup(String result) {
        BatteryInfoService.startDrain = true;

        ShowResult.showLog("inside show drain popup Message = " + result);
        Intent in = new Intent().setClass(getBaseContext(), ConfirmDrainActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.putExtra("msg", result);
        in.putExtra("action", "report");
        startActivity(in);


    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    public String urlBuilder(String baseUrl, String deviceid, String secret, long starttime, long endtime, int startbattery, int endbattery) {
        return baseUrl + "?deviceid=" + deviceid + "&secret=" + secret + "&starttime=" + starttime + "&endtime=" + endtime + "&startbattery=" + startbattery + "&endbattery=" + endbattery;
    }

    // get user id from aware framework for uniformity of user id
    private String getUserId() {
        String auri = "content://com.aware.provider.aware/aware_device";
        Uri u = Uri.parse(auri);
        Cursor esm_data = getContentResolver().query(u, null, null, null, null);

        if (esm_data != null && esm_data.moveToLast()) {
            String uid = esm_data.getString(esm_data.getColumnIndex("device_id"));
            ShowResult.showLog(uid);
            return uid;

        }
        return null;
    }

    // use async task to submit draining report, because it involve call to server which can delay for few seconds so we have to execute it in background.
    private class MyAsyncClientSubmitReport extends AsyncTask<String, String, String> {

        //Submit drain report on server return string as response from server
        @Override
        protected String doInBackground(String... params) {
            String uri = params[0];
            Calendar currentTime = cal.getInstance();


            String startTime = sdf.format(currentTime.getTime());
            // get response from server using MyHttpManager class.
            String respFromServer = MyHttpManager.getData(uri);

            int retrycount = 0;
            ShowResult.showLog("Submit Drain Report " + uri);
            // if connection with server is not successfully build then retry till 30 seconds
            while (respFromServer.equalsIgnoreCase("notconnected") && retrycount < 30) {
                retrycount = retrycount + 1;
                try {
                    Thread.sleep(60 * 1000);
                    ShowResult.showLog("Exception in connection " + respFromServer + " retry count= " + retrycount);
                    respFromServer = MyHttpManager.getData(uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return respFromServer;
        }

        // when our background task (doInBackground) complete its work it will send response to onPostExecute and we then return that response to App UI
        @Override
        protected void onPostExecute(String result) {
            //Parse Json response from server.
            ConDrainJSONParser jp = new ConDrainJSONParser();
            ConDrainRespDto rd = jp.parseResp(result);
            String confirmMsg;
//            confirmMsg=rd.getSuccess()+"\nBid Value="+rd.getBidval()+"\nTotal Amount won till now ="+rd.getTally();
            confirmMsg = "Battery drain report submitted successfully. \nBid Value=" + rd.getBidval() + "\nTotal Amount won till now =" + rd.getTally();
            //If our application fails to make communication with server then we will get null in bid value so we show general error report on application UI
            if (rd.getBidval() == null || rd.getBidval().equalsIgnoreCase("null")) {
                confirmMsg = "Network problem: Unable to submit confirm drain report, please contact administrator of the game";
            }

            showDrainReportPopup(confirmMsg);
            //			super.onPostExecute(result);
        }

    }
}
