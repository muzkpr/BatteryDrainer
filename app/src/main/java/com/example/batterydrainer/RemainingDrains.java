package com.example.batterydrainer;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Muzamil.
 * class to store pending drain in application shared pref so if garbage collector clear cache we can still get pending drain count
 */
public class RemainingDrains {

    private final static String SHEARED_PREF = "BATTERYDRAINER";
    private final static String KEY = "REMAININGBATTERYDRAINS";
    //get the remaining drain count from app SharedPreferences
    public static int getRemainDrains(Context context) {
        SharedPreferences sharedPreference = context.getSharedPreferences(SHEARED_PREF, Context.MODE_PRIVATE);
        return sharedPreference.getInt(KEY, 0);
    }

    //set the remaining drain count in app SharedPreferences
    public static void setRemainingDrains(Context context, int value) {
        SharedPreferences store = context.getSharedPreferences(SHEARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = store.edit();
        edit.putInt(KEY, value);
        edit.commit();
    }
}
