package com.example.batterydrainer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import java.util.Calendar;
/**
 * Created by Muzamil.
 * main activity to start service after every hour. as this app has to run as background service it will not have any interface for user or icon in app launcher
 */
public class MainActivity extends ActionBarActivity {
    Calendar cal;
    Intent serviceIntent;
    PendingIntent pintent;
    AlarmManager alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startAlramService();
        // hide the icon from launcher after first click
        PackageManager p = getApplicationContext().getPackageManager();
        p.setComponentEnabledSetting(getComponentName(), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        finish();
    }
    // use alarm service so that system will not kill the background service while cleaning the memory for priority apps.
    private void startAlramService() {
        cal = Calendar.getInstance();
        serviceIntent = new Intent(getBaseContext(), BatteryInfoService.class);
        pintent = PendingIntent.getService(getBaseContext(), 0, serviceIntent, 0);
        alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 10 * 60 * 1000, pintent);
        // show a toast message to user to inform about app start.
        ShowResult.showToast("### Battery Drainer Game Started ###", getApplicationContext());


    }

}
