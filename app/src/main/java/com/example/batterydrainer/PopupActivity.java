package com.example.batterydrainer;


import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
/**
 * Created by Muzamil.
 * activity to show popup after winning the bid
 */
public class PopupActivity extends ActionBarActivity {
    Button btOk;
    TextView tvMsg;
    String action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // play notification sound while displaying popup
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // show activity window as popup window
        setContentView(R.layout.activity_popup);
        Intent intent = getIntent();
        String message = intent.getStringExtra("msg");
        action = intent.getStringExtra("action");

        tvMsg = (TextView) findViewById(R.id.tvMsg);
        tvMsg.setText(message);
        btOk = (Button) findViewById(R.id.btPopOk);
        btOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (action.equalsIgnoreCase("drain")) {
                    startService(new Intent(getBaseContext(), BatteryDrainerService.class));

                } else if (action.equalsIgnoreCase("anotherdrain")) {
                    startService(new Intent(getBaseContext(), BatteryDrainerService.class));

                }
                finish();
            }
        });
    }


}
