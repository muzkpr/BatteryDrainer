package camera;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
/**
 * Created by Muzamil.
 * use camera only to drain mobile battery
 */
public class UseCamera {
    Camera cam;

    public boolean startCamera() {
        try {
            //open camera
            cam = Camera.open();
            Parameters p = cam.getParameters();
            cam.setParameters(p);
            //start preview but don't save any video
            cam.startPreview();
            cam.unlock();
            return true;
        } catch (Exception e) {
//			Log.d("Inspect", "exception in starting video " + e)
            e.printStackTrace();

        }

        return false;
    }
    //stop camera
    public void stopCamera() {
        cam.stopPreview();
        cam.lock();
        cam.release();
    }
}
