package audio;


import android.media.MediaRecorder;
import android.os.Environment;

import com.example.batterydrainer.ShowResult;

import java.io.File;
import java.io.IOException;
/**
 * Created by Muzamil.
 * use microphone to drain mobile battery
 */
public class UseAudio {

    final MediaRecorder recorder = new MediaRecorder();
    final String path;


    /**
     * Creates a new audio recording at the given path (relative to root of SD card).
     */
    public UseAudio(String path) {
        this.path = sanitizePath(path);
    }

    // set path to store audio file
    private String sanitizePath(String path) {
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        if (!path.contains(".")) {
            path += "";
        }
        return Environment.getExternalStorageDirectory().getAbsolutePath() + path;
    }

    /**
     * Starts a new recording.
     */
    public void start() throws IOException {
        String state = Environment.getExternalStorageState();
        if (!state.equals(Environment.MEDIA_MOUNTED)) {
            throw new IOException("SD Card is not mounted.  It is " + state + ".");
        }

        // make sure the directory we plan to store the recording in exists
        File directory = new File(path).getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            throw new IOException("Path to file could not be created.");
        }
        // set parameter for audio recoding
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(path);
        recorder.prepare();
        recorder.start();
    }

    /**
     * Stops a recording that has been previously started.
     */
    public void stop() throws IOException {
        File file = new File(path);
        // delete the recorded file because we dont want to store that audio
        boolean deleted = file.delete();
        ShowResult.showLog("file deleted =" + deleted);
        // stop recording and release microphone
        recorder.stop();
        recorder.release();
    }

}