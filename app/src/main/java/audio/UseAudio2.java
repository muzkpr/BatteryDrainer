package audio;


import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import com.example.batterydrainer.ShowResult;
/**
 * Created by Muzamil.
 * use microphone to drain mobile battery
 */
public class UseAudio2 {
    AudioRecord record;
    // start using mircophone without storing audio file
    public void start() {
        try {
            //set parameter for audio recoding
            int min = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
            record = new AudioRecord(MediaRecorder.AudioSource.VOICE_COMMUNICATION, 8000, AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, min);
            //start recording
            record.startRecording();
            ShowResult.showLog("going to start Micro Phone");
        } catch (Exception e) {
            ShowResult.showLog("exception in start recording" + e);
        }
    }
    // stop using microphone
    public void stop() {
        try {
            ShowResult.showLog("going to stop Micro Phone");

            record.stop();
        } catch (Exception e) {
            ShowResult.showLog("exception in stop recording" + e);

        }
    }
}
