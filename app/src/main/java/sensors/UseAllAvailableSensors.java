package sensors;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import com.example.batterydrainer.ShowResult;
/**
 * Created by Muzamil.
 * use all available sensors on mobile hand set to drain mobile battery.
 * use this class if not sure which sensors are available on the mobile set.
 * use this class if want to drain mobile battery only.
 */
public class UseAllAvailableSensors extends Service implements SensorEventListener {

    long lastUpdate;
    String data;
    // reference to sensor manager.
    private SensorManager sensorManager;

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ShowResult.showLog("Going to start UseAllAvailableSensors");
        // get system sensor services
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ALL) != null) {
            // register listener for all available sensors on mobile set, with fastest possible frequency.
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ALL),
                    SensorManager.SENSOR_DELAY_FASTEST);

            ShowResult.showLog("Sensors are available");

        } else {
            ShowResult.showLog("sensors are not available");
        }


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (sensorManager != null) {
            ShowResult.showLog("going to close sensor manager");
            sensorManager.unregisterListener(this);

        } else {
            ShowResult.showLog("sensors manager is null");

        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
    // check if any event happened for all available sensors.
    @Override
    public void onSensorChanged(SensorEvent event) {

        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            getAccelerometer(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
            getAmbientTemperature(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR) {
            getGameRotation(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR) {
            getGeoMagnaticRotation(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_GRAVITY) {
            getGravity(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            getGyroscope(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
            getHeartRate(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            getLight(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            getLinearAcceleration(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            getMaganeticField(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
            getMaganeticFieldUnCalibrated(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_ORIENTATION) {
            getOrientation(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_PRESSURE) {
            getPressure(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            getProximity(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY) {
            getRelativeHumidity(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            getRotationVector(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_SIGNIFICANT_MOTION) {
            getSignificantMotion(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            getStepCounter(event);

        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            getStepDetector(event);
        }
        // check if specific sensor is available then get its data
        if (event.sensor.getType() == Sensor.TYPE_TEMPERATURE) {
            getTemperature(event);
        }
        // check if specific sensor is available then get its data

    }

    private void getTemperature(SensorEvent event) {
        float x = event.values[0];

    }

    private void getStepDetector(SensorEvent event) {
        float x = event.values[0];


    }

    private void getStepCounter(SensorEvent event) {
        float x = event.values[0];


    }

    private void getSignificantMotion(SensorEvent event) {
        float x = event.values[0];


    }

    private void getRotationVector(SensorEvent event) {
        float x = event.values[0];


    }

    private void getRelativeHumidity(SensorEvent event) {
        float x = event.values[0];


    }

    private void getProximity(SensorEvent event) {
        float x = event.values[0];


    }

    private void getPressure(SensorEvent event) {
        float x = event.values[0];


    }

    private void getOrientation(SensorEvent event) {
        float x = event.values[0];

    }

    private void getMaganeticFieldUnCalibrated(SensorEvent event) {
        float x = event.values[0];

    }

    private void getMaganeticField(SensorEvent event) {
        float x = event.values[0];

    }

    private void getLinearAcceleration(SensorEvent event) {
        float x = event.values[0];

    }

    private void getLight(SensorEvent event) {
        float x = event.values[0];

    }

    private void getHeartRate(SensorEvent event) {
        float x = event.values[0];

    }

    private void getGyroscope(SensorEvent event) {
        float x = event.values[0];

    }

    private void getGeoMagnaticRotation(SensorEvent event) {
        float x = event.values[0];

    }

    private void getGameRotation(SensorEvent event) {
        float x = event.values[0];

    }

    private void getAmbientTemperature(SensorEvent event) {
        float x = event.values[0];

    }

    private void getAccelerometer(SensorEvent event) {
        float x = event.values[0];

    }

    private void getGravity(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }

}
